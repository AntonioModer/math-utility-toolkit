#include "framework.h"
#include "tests.h"

using namespace framework;

void cTest1::init( void )
{
    setTitle( "ray box intersection" );
    setNumPoints( 4 );
}

void cTest1::tick( void ) 
{
    // draw the ray
    draw::colour( 0xf0c080 );
    draw::arrow( point[0], point[1], 8 );
    // find the box
    mut::box2 box( mut::minv( point[2], point[3] ), mut::maxv( point[2], point[3] ) );
    // draw the rect
    draw::rect( box );
    // find the ray
    mut::ray2 ray( point[0], mut::normalize( point[1] - point[0] ) );
    // raybox hitpoint
    mut::vec2 hit;
    // do ray box intersect check
    if ( mut::intersect( ray, box, hit ) )
    {
        mut::circle2 circle( hit, 8.0f );
        draw::colour( 0xffffff );
        draw::circle( circle );
    }
}
