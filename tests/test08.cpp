#include "framework.h"
#include "tests.h"

using namespace framework;

static mut::vec2 current1;
static mut::vec2 current2;

void cTest8::init( void )
{
    setTitle( "circle edge intersection" );
    setNumPoints( 4 );
}

void cTest8::tick( void ) 
{
    draw::colour( 0x5890d0 );
    draw::line( point[0], point[1] );

    float r = mut::distance( point[2], point[3] );

    mut::circle2 circ( point[2], r );
    mut::edge2   edge( point[0], point[1] );
    

    mut::vec2 i1, i2;
    if ( mut::intersect( circ, edge, i1, i2 ) )
    {
        draw::colour( 0xffffff );
        draw::circle( mut::circle2( i1, 8 ) );
        draw::circle( mut::circle2( i2, 8 ) );

        draw::colour( 0xff0000 );
    }
    else
        draw::colour( 0x5890d0 );


    draw::circle( circ );
}
