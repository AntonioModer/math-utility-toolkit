#include "framework.h"
#include "tests.h"

using namespace framework;

void cTest2::init( void )
{
    setTitle( "cosine interpolation" );
    setNumPoints( 6 );
}

static
void curve
(
    const mut::vec2 &a,
    const mut::vec2 &b,
    const mut::vec2 &c,
    const mut::vec2 &d
)
{
    float cx[4] = { a.x, b.x, c.x, d.x };
    float cy[4] = { a.y, b.y, c.y, d.y }; 

    mut::vec2 lt = b;
    
    float step  = 0.1f;
    for ( float i=0.f; i<=1.f+step; i+= step )
    {
        mut::vec2 it = mut::vec2
            ( mut::interp_cubic( cx, i ),
              mut::interp_cubic( cy, i ) );

        draw::line( lt, it );
        lt = it;
    }
}

void cTest2::tick( void ) 
{
    draw::colour( 0xffffff );
    curve( point[0], point[0], point[1], point[2] );
    curve( point[0], point[1], point[2], point[3] );
    curve( point[1], point[2], point[3], point[4] );
    curve( point[2], point[3], point[4], point[5] );
    curve( point[3], point[4], point[5], point[5] );
}
