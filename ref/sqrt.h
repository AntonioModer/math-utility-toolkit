#pragma once
#include "types.h"

/*
// 
static uint32 _isqrt1( uint32 i )
{
	int max;
	int j;
	unsigned int a, c, d, s;
	a=0; c=0;
	if ( i >= 0x10000 )
		max=15;
	else
		max = 7;
	s = 1<<(max*2);
	j = max;
	do
	{
		d = c + (a<<(j+1)) + s;
		if ( d <= i )
		{
			c=d;
			a |= 1 << j;
		};
		if ( d != i )
		{
			s >>= 2;
			j--;
		} 
	}
	while ((j>=0) && (d!=i));
	return a;
}

//
static uint32 _isqrt2( uint32 n )
{
	uint32 root = 0, bit = 0, trial = 0;
	bit = (n >= 0x10000) ? 1<<30 : 1<<14;
	do
	{
		trial = root+bit;
		if (n >= trial)
		{
			n -= trial;
			root = trial+bit;
		}
		root >>= 1;
		bit  >>= 2;
	} while ( bit );
	return root;
}
*/

#define BITSPERLONG 32

#define TOP2BITS(x) ((x & (3L << (BITSPERLONG-2))) >> (BITSPERLONG-2))


/*

    ENTRY x: unsigned long
    EXIT  returns floor(sqrt(x) * pow(2, BITSPERLONG/2))

    Since the square root never uses more than half the bits
    of the input, we use the other half of the bits to contain
    extra bits of precision after the binary point.

    EXAMPLE
        suppose BITSPERLONG = 32
        then    usqrt(144) = 786432 = 12 * 65536
                usqrt(32) = 370727 = 5.66 * 65536

    NOTES
        (1) change BITSPERLONG to BITSPERLONG/2 if you do not want
            the answer scaled.  Indeed, if you want n bits of
            precision after the binary point, use BITSPERLONG/2+n.
            The code assumes that BITSPERLONG is even.
        (2) This is really better off being written in assembly.
            The line marked below is really a "arithmetic shift left"
            on the double-long value with r in the upper half
            and x in the lower half.  This operation is typically
            expressible in only one or two assembly instructions.
        (3) Unrolling this loop is probably not a bad idea.

    ALGORITHM
        The calculations are the base-two analogue of the square
        root algorithm we all learned in grammar school.  Since we're
        in base 2, there is only one non trivial trial multiplier.

        Notice that absolutely no multiplications or divisions are performed.
        This means it'll be fast on a wide range of processors.
*/
/*
void usqrt( unsigned long x, struct int_sqrt *q )
{
      unsigned long a = 0L; // accumulator
      unsigned long r = 0L; // remainder
      unsigned long e = 0L; // trial product

      int i;

      for (i = 0; i < BITSPERLONG; i++) // NOTE 1
      {
            r = (r << 2) + TOP2BITS(x); x <<= 2; // NOTE 2
            a <<= 1;
            e = (a << 1) + 1;
            if (r >= e)
            {
                  r -= e;
                  a++;
            }
      }
      memcpy( q, &a, sizeof(long) );
}

main(void)
{
      int i;
      unsigned long l = 0x3fed0169L;
      struct int_sqrt q;

      for (i = 0; i < 101; ++i)
      {
            usqrt(i, &q);
            printf("sqrt(%3d) = %2d, remainder = %2d\n",
                  i, q.sqrt, q.frac);
      }
      usqrt(l, &q);
      printf("\nsqrt(%lX) = %X, remainder = %X\n", l, q.sqrt, q.frac);
      return 0;
}
*/


/*
uint16_t int_sqrt32(uint32_t x)
{
    uint16_t res=0;
    uint16_t add= 0x8000;   
    int i;
    for(i=0;i<16;i++)
    {
        uint16_t temp=res | add;
        uint32_t g2=temp*temp;      
        if (x>=g2)
        {
            res=temp;           
        }
        add>>=1;
    }
    return res;
}
*/

// single iteration step
inline void _i( uint32 N, uint32 &n, uint32 &root, uint32 &tx )
{
    tx = root + ( 1 << N );
    if ( n >= ( tx << N ) )
    {
		n    -= tx << N;	
        root |= 2  << N;
    }
}
inline uint32 _isqrt3( uint32 n )
{
    uint32 r = 0, x = 0;
    _i(15,n,r,x); _i(14,n,r,x); _i(13,n,r,x); _i(12,n,r,x);
    _i(11,n,r,x); _i(10,n,r,x); _i( 9,n,r,x); _i( 8,n,r,x);
    _i( 7,n,r,x); _i( 6,n,r,x); _i( 5,n,r,x); _i( 4,n,r,x);
    _i( 3,n,r,x); _i( 2,n,r,x); _i( 1,n,r,x); _i( 0,n,r,x);
	// 
    return r >> 1;
}

// 
inline uint32 isqrt( uint32 n )
{
	return _isqrt3( n );
}